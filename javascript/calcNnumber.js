function Driver(Name,Distance,PointA,PointB)
{
	var name = Name;
	var distance = -1;
	var pointA = PointA;
	var pointB = PointB;
	var detourDistance = -1;
	return [name,distance,pointA,pointB,detourDistance];
}

function Point(x,y)
{
	return [x,y];
}

function Distance(PointA,PointB)
{
	var dx = Math.abs(PointA[0]-PointB[0]);

	var dy = Math.abs(PointA[1]-PointB[1]);

	var d = Math.sqrt(Math.pow(dx,dx)+Math.pow(dy,dy));

	return Math.floor(d);
}

function RandomPoint() {
  var x0 = 24;
  var y0 = 23;
  var radius = 1000000;
  // Convert Radius from meters to degrees.
  var rd = radius/111300;
 
  var u = Math.random();
  var v = Math.random();
 
  var w = rd * Math.sqrt(u);
  var t = 2 * Math.PI * v;
  var x = w * Math.cos(t);
  var y = w * Math.sin(t);
 
  var xp = x/Math.cos(y0);
 
  return [y+y0,x+x0];
}

function MinTour(Drivers)
{
	var min = Number.NEGATIVE_INFINITY;
	var index = -1;

	for(var i=0; i<Drivers.length-1; i++)
	{
		if(min == Number.NEGATIVE_INFINITY)
		{
			min = Drivers[i][1];
			index = i;
		}
		else if (min > Drivers[i][1])
		{
			main = Drivers[i][1];
			index = i;
		}
	}
	return Drivers[index];
}

function Total()
{
	var Drivers = [];
	var D1 = new Driver('Chris',-1,RandomPoint(),RandomPoint());
	var D2 = new Driver('Michael',-1,RandomPoint(),RandomPoint());
	var D3 = new Driver('Rachel',-1,RandomPoint(),RandomPoint());
	var D4 = new Driver('Joy',-1,RandomPoint(),RandomPoint());
	Drivers.push(D1);
	Drivers.push(D2);
	Drivers.push(D3);
	Drivers.push(D4);

	Detour = [];

	Drivers.forEach(function (Driver) {
		CalcTourDistance(Driver);
	});

	console.log(Drivers);
	console.log(MinTour(Drivers));

	//Drivers.forEach(function (Driver){
		Detour.push(CalcDetourDistance(Drivers[0],Drivers));
		console.log(Detour,'detour');
	//});
}

function CalcTourDistance(mainDriver)
{
	mainDriver[1] = Distance(mainDriver[2],mainDriver[3]);
}

function CalcDetourDistance(mainDriver,Drivers)
{
	locked = [];
	unlocked = [];
	var startPoint = mainDriver[2];
	var length = 0;

	Drivers.forEach(function (Driver) {	
		if(Driver != mainDriver) {
			locked.push([Driver[3],Driver[0]]);
			unlocked.push([Driver[2],Driver[0]]);
		}
	});

	var counter = 1;
	var index = null;
	var identifier = null;
	var min = null;
	var prevID = null;
	var prevPoint = null;

	do
	{
		for(y=0; y<unlocked.length-1; y++) {
			for(i=0; i<unlocked.length-1; i++)
			{
				if(min == null)
				{
					identifier = unlocked[i][1];
					index = unlocked[i][0];
					min = Distance(startPoint,index);
				}
				else if(min > Distance(startPoint,unlocked[i][0]))
				{
					identifier = unlocked[i][1];
					index = unlocked[i][0];
					min = Distance(startPoint,index);
				}
			};

			startPoint = index;
			length += min;

			for(i=0; i<locked.length-1; i++)
			{
				console.log(identifier);
				if(locked[i][1] == identifier)
				{

					unlocked.push(locked[i]);
					locked.splice( i, 1 );
					i = locked.length;
				}
			}

			//TODO add code to remove element from unlocked so it doesnt loop.
		};
		counter++;
	}
	while(locked.length > 0 && counter < 20 ) //(safety)

}


Total();
function Driver(Name,Distance,PointA,PointB)
{
	var name = Name;
	var distance = -1;
	var pointA = PointA;
	var pointB = PointB;
	var detourDistance = -1;
	return [name,distance,pointA,pointB,detourDistance];
}

function Point(x,y)
{
	return [x,y];
}

function Distance(PointA,PointB)
{
	var dx = Math.abs(PointA[0]-PointB[0]);

	var dy = Math.abs(PointA[1]-PointB[1]);

	var d = Math.sqrt(Math.pow(dx,dx)+Math.pow(dy,dy));

	return Math.floor(d);
}

function RandomPoint() {
  var x0 = 24;
  var y0 = 23;
  var radius = 1000000;
  // Convert Radius from meters to degrees.
  var rd = radius/111300;
 
  var u = Math.random();
  var v = Math.random();
 
  var w = rd * Math.sqrt(u);
  var t = 2 * Math.PI * v;
  var x = w * Math.cos(t);
  var y = w * Math.sin(t);
 
  var xp = x/Math.cos(y0);
 
  // Resulting point.
  //return {'lat': y+y0, 'lng': xp+x0};
  return [y+y0,x+x0];
}

function Init()
{
	Total();
}

function MinTour(Drivers)
{
	var min = Number.NEGATIVE_INFINITY;
	var index = -1;

	for(var i=0; i<Drivers.length-1; i++)
	{
		if(min == Number.NEGATIVE_INFINITY)
		{
			min = Drivers[i][1];
			index = i;
		}
		else if (min > Drivers[i][1])
		{
			main = Drivers[i][1];
			index = i;
		}
	}
	return Drivers[index];
}

function Total()
{
	var Drivers = [];
	var D1 = new Driver('Chris',-1,RandomPoint(),RandomPoint());
	var D2 = new Driver('Michael',-1,RandomPoint(),RandomPoint());
	Drivers.push(D1);
	Drivers.push(D2);

	Detour = [];

	Drivers.forEach(function (Driver) {
		CalcTourDistance(Driver);
	});

	console.log(Drivers);
	console.log(MinTour(Drivers));


	Detour.push(CalcDetourDistance(Drivers[0],Drivers));
	console.log(Detour,'detour');

}

function CalcTourDistance(mainDriver)
{
	mainDriver[1] = Distance(mainDriver[2],mainDriver[3]);
}

function CalcDetourDistance()
{
	var Drivers = [];

	Drivers.push(new Driver('Chris',
							0,
							 new Point(
								 $("#d1").find("#pointAlat").val(),
							   	 $("#d1").find("#pointAlon").val()
							 ),
							 new Point(
								 $("#d1").find("#pointBlat").val(),
							   	 $("#d1").find("#pointBlon").val()
							 )
						   ));

	Drivers.push(new Driver('Vicky',
							0,
							 new Point(
								 $("#d2").find("#pointAlat").val(),
							   	 $("#d2").find("#pointAlon").val()
							 ),
							 new Point(
								 $("#d2").find("#pointBlat").val(),
							   	 $("#d2").find("#pointBlon").val()
							 )
						   ));



	var Dif1 = Distance(Drivers[0][2],Drivers[1][2]) + 
			   Distance(Drivers[1][2],Drivers[1][3]) +
			   Distance(Drivers[1][3],Drivers[0][3]) -
			   Distance(Drivers[0][2],Drivers[0][3]);
	
	var Dif2 = Distance(Drivers[1][2],Drivers[0][2]) + 
			   Distance(Drivers[0][2],Drivers[0][3]) +
			   Distance(Drivers[0][3],Drivers[1][3]) -
			   Distance(Drivers[1][2],Drivers[1][3]);

	if(Dif1>Dif2)
	{	return [[Drivers[0],Dif1],[Drivers[1],Dif2],Drivers[1]]; }
	else 
	{	return [[Drivers[0],Dif1],[Drivers[1],Dif2],Drivers[0]]; }

	
}
function init()
{
  var blCalc = false;

  $("#home-btn").click(function (){
    $("#info").show(300);
    $("#about-btn").parent().removeClass("active"); 
    $("#home-btn").parent().addClass("active");   
    $("#aboutme-panel").hide(300);
  });
  
  $("#about-btn").click(function (){
    console.log('test');
    $("#info").toggle(300);
    $("#about-btn").parent().toggleClass("active"); 
    $("#home-btn").parent().toggleClass("active");   
    $("#aboutme-panel").toggle(300);

    $("#sol-panel").hide(0);
    $("#sol-exp-panel").hide(0);

  });
  
  $("#sol").click(function (){
    $("#sol-panel").toggle(300);
    $("#sol-exp-panel").toggle(400);
    $('html, body').animate({
        scrollTop: $("#sol-panel").offset().top
    }, 200);
  });

  $("#randomd1").click(function (){
    var randomPointX = RandomPoint();
    var randomPointY = RandomPoint();

    $("#d1").find("#pointAlat").val(randomPointX[0]);
    $("#d1").find("#pointAlon").val(randomPointX[1]);
    $("#d1").find("#pointBlat").val(randomPointY[0]);
    $("#d1").find("#pointBlon").val(randomPointY[1]);
  });

  $("#randomd2").click(function (){
    var randomPointX = RandomPoint();
    var randomPointY = RandomPoint();

    $("#d2").find("#pointAlat").val(randomPointX[0]);
    $("#d2").find("#pointAlon").val(randomPointX[1]);
    $("#d2").find("#pointBlat").val(randomPointY[0]);
    $("#d2").find("#pointBlon").val(randomPointY[1]);
  });



  $("#calc").click(function (){
    var errors = 0;
    $("#d1 :input").map(function(){
         if( !$(this).val() ) {
              $(this).addClass('warning');
              errors++;
        } else if ($(this).val()) {
              $(this).removeClass('warning');
        }   
    });

    if(errors == 0){
      $("#result").toggle(200);
      var result = CalcDetourDistance();
      $("#result-p").text("Driver 1: " + result[0][0][0] + " has a detour difference of: " + result[0][1] +
                          " While Driver 2: " + result[1][0][0] + " has a detour difference of: " + result[1][1]);

      $("#winner").text("The Winner is :" + result[2][0] );
    }
    else
    {
      $("#result").toggle(200);
      $("#result-p").text("Please fill in the required values.");
      
    }

    $("#calc").text("Re-Calculate");


  });
}